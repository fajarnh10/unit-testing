package com.fajar;
import java.util.Scanner;
public class Uap3b {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("      RA.Al-Karomah Bogor");
		System.out.println("Silahkan Pilih bayaran Yang ingin dibayar !!!");
		System.out.println("1). PENGEMBANGAN\n2). SPP\n3). KEGIATAN\n4). RAPORT");
		System.out.println("---------------------------------------------------------------------------------");
		System.out.print("Apakah Ingin Membayar PENGEMBANGAN ( pilih 1 jika ya ,pilih 2 jika tidak ) : ");
		int isPengembangan = scanner.nextInt();
		System.out.print("Apakah Ingin Membayar SPP ( pilih 1 jika ya ,pilih 2 jika tidak ) : ");
		int isSpp = scanner.nextInt();
		System.out.print("Apakah Ingin Membayar UANG KEGIATAN ( pilih 1 jika ya ,pilih 2 jika tidak ) : ");
		int isKegiatan = scanner.nextInt();
		System.out.print("Apakah Ingin Membayar RAPORT ( pilih 1 jika ya ,pilih 2 jika tidak ) : ");
		int isRaport = scanner.nextInt();
		System.out.println("---------------------------------------------------------------------------------");
		String notice = null;
		int totalBayar = 0, pengembangan = 3000000, spp = 1200000, kegiatan = 3500000, 
				raport = 500000;
		if (isPengembangan == 1) {
			if (isSpp == 1) {
				if (isKegiatan == 1) {
					if (isRaport == 1) {
						notice = "Terima Kasih Telah Melunasi Semua";
						totalBayar = pengembangan + spp + kegiatan + raport;
					}else if (isRaport == 2) {
						notice = "Uang raport belum Rp. 500.000";
						totalBayar = pengembangan + spp + kegiatan;
					}
				}else if (isKegiatan == 2) {
					if (isRaport == 1) {
						notice = "Uang kegiatan belum Rp.3.500.000";
						totalBayar = pengembangan + spp + raport;
					}else if (isRaport == 2) {
						notice = "Uang kegiatan Rp.3.500.000 dan raport belum Rp.500.000";
						totalBayar = pengembangan + spp;
					}
				}
			}else if (isSpp == 2) {
				if (isKegiatan == 1) {
					if (isRaport == 1) {
						notice = "Uang spp belum Rp.1.200.000";
						totalBayar = pengembangan + kegiatan + raport;
					}else if (isRaport == 2) {
						notice = "Uang spp Rp.1.200.000 dan raport belum Rp.500.000";
						totalBayar = pengembangan + kegiatan;
					}
				}else if (isKegiatan == 2) {
					if (isRaport == 1) {
						notice = "Uang spp Rp.1.200.000 dan kegiatan belum Rp.3.500.000";
						totalBayar = pengembangan + raport;
					}else if (isRaport == 2) {
						notice = "Uang spp Rp.1.200.000 , kegiatan Rp.3.500.000 dan raport "
								+ "belum Rp.500.000";
						totalBayar = pengembangan;
					}
				}
			}
		}else if (isPengembangan == 2) {
			if (isSpp == 1) {
				if (isKegiatan == 1) {
					if (isRaport == 1) {
						notice = "Uang pengembangan belum Rp.3.000.000";
						totalBayar = pengembangan + kegiatan + raport;
					}else if (isRaport == 2) {
						notice = "Uang pengembangan Rp.3.000.000 dan raport belum Rp.500.000";
						totalBayar = spp + kegiatan;
					}
				}else if (isKegiatan == 2) {
					if (isRaport == 1) {
						notice = "Uang pengembangan Rp.3.000.000 dan kegiatan belum Rp.3.500.000";
						totalBayar = spp + raport;
					}else if (isRaport == 2) {
						notice = "Uang pengembangan Rp.3.000.000 , kegiatan Rp.3.500.000 "
								+ "dan raport belum Rp.500.000";
						totalBayar = spp;
					}
				}
			}else if (isSpp == 2) {
				if (isKegiatan == 1) {
					if (isRaport == 1) {
						notice = "Uang pengembangan Rp.3.000.000 dan  spp belum Rp.1.200.000";
						totalBayar = kegiatan + raport;
					}else if (isRaport == 2) {
						notice = "Uang pengembangan Rp.3.000.000,spp Rp.1.200.000 dan raport belum Rp.500.000";
						totalBayar = kegiatan;
					}
				}else if (isKegiatan == 2) {
					if (isRaport == 1) {
						notice = "Uang pengembangan Rp.3.000.000,spp Rp.1.200.000 dan kegiatan belum Rp.3.500.000";
						totalBayar = raport;
					}else if (isRaport == 2) {
						notice = "Uang pengembangan , spp, kegiatan dan raport belum HARAP LUNASI SEGERA !!!!";
					}
				}
			}
		}else {
			notice = "Pilihan tidak ditemukan !!!";
		}
		
		System.out.println(notice);
		if (totalBayar > 0) {
			System.out.println("TOTAL YANG SUDAH DIBAYARKAN = Rp. " + totalBayar);
		}
	}
}
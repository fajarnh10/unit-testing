package com.fajar;
import java.util.Scanner;
public class Uap {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("=== PERHITUNGAN Penjualan Baju Gamis ( Selama Ramadhan 141 H ) - "
				+ "GAMIS FASHION ===");
		System.out.println("|==============================================================|");
		System.out.print("Silahkan Input jumlah Penjualan Baju Gamis Minggu Pertama : ");
		int mingguPertama = scanner.nextInt();
		System.out.println("|==============================================================|");
		System.out.print("Silahkan Input jumlah Penjualan Baju Gamis Sampai Minggu Terakhir : ");
		int mingguTerakhir = scanner.nextInt();
		System.out.println("|==============================================================|");
		System.out.println("|                          |                                   |");
		System.out.println("|                          |                                   |");
		System.out.println("V                          V                                   V");
		System.out.println();
		int totalPenjualan = mingguPertama+mingguTerakhir;
		System.out.println("        Total Penjualan adalah = " + totalPenjualan);
		System.out.println();
		if (totalPenjualan == 100) {
			System.out.println("Anda Mencapai Target Penjualan Sebulan");
		}else if (totalPenjualan < 100) {
			System.out.println("Anda Tidak Mencapai Target Penjualan Sebulan");
		}else if (totalPenjualan > 100) {
			System.out.println("Anda Melebihi Mencapai Target Penjualan Sebulan");
		}
		System.out.println("===============================");
		System.out.println("Dengan rincian Harga Modal Rp.80.000, Dijual Rp.150.000");
		int modal = 80000*totalPenjualan;
		int keuntungan = (totalPenjualan*150000) - modal;
		System.out.println("MODAL = " + modal + " Rupiah");
		System.out.println("KEUNTUNGAN = " + keuntungan + " Rupiah");
	}
}
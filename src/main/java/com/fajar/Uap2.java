package com.fajar;
import java.util.Scanner;
public class Uap2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("              BELITIKET DOT COM");
		System.out.println("Silahkan Pilih Nama Pesawat Tang Diinginkan !!!");
		System.out.println("1). Garuda\n2). Lion\n3). Citilink");
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		System.out.print("Silahkan Pilih ( [1] atau [2] atau [3] ) : ");
		int pilihan = scanner.nextInt();
		System.out.print("Masukan Jumlah Tiket Yang Ingin Di Beli : ");
		int jumlahTiket = scanner.nextInt();
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		int diskonHarga = 0, totalHarga = 0, hargaTiket = 0;
		String pesawat = null;
		boolean pilihanValid = true;
		if (pilihan == 1) {
			if (jumlahTiket >= 100) {
				hargaTiket = 400000;
				pesawat = "Garuda-Diskon 50%";
				diskonHarga = 200000;
				totalHarga = (hargaTiket-diskonHarga)*jumlahTiket;
			}else {
				hargaTiket = 400000;
				pesawat = "Garuda-Tidak ada Diskon";
				totalHarga = hargaTiket*jumlahTiket;
			}
		}else if (pilihan == 2) {
			if (jumlahTiket >= 100) {
				hargaTiket = 200000;
				pesawat = "Lion-Diskon 50%";
				diskonHarga = 100000;
				totalHarga = (hargaTiket-diskonHarga)*jumlahTiket;
			}else {
				hargaTiket = 200000;
				pesawat = "Lion-Tidak ada Diskon";
				totalHarga = hargaTiket*jumlahTiket;
			}
		}else if (pilihan == 3) {
			if (jumlahTiket >= 100) {
				hargaTiket = 300000;
				pesawat = "Citilink-Diskon 50%";
				diskonHarga = 150000;
				totalHarga = (hargaTiket-diskonHarga)*jumlahTiket;
			}else {
				hargaTiket = 300000;
				pesawat = "Citilink-Tidak ada Diskon";
				totalHarga = hargaTiket*jumlahTiket;
			}
		}else {
			pilihanValid = false;
		}
		if (pilihanValid) {
			System.out.println("Nama Pesawat : " + pesawat);
			System.out.println("Harga Tiket : " + hargaTiket);
			System.out.println("DISKON : " + diskonHarga);
			System.out.println("TOTAL : " + totalHarga);
		}else {
			System.out.println("Pilihan anda tidak dapat di temukan !");
		}
	}
}
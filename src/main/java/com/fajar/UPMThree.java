package com.fajar;
import java.util.Scanner;
public class UPMThree {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("APLIKASI TOKO");
		System.out.println(">>>>>>>>>>o<<<<<<<<<<");
		System.out.print("Masukan Jumlah Baranng Yang Di Beli : ");
		int inputItems = scanner.nextInt();
		String[] itemsArr = new String[inputItems];
		int[] amountArr = new int[inputItems];
		System.out.println("------------------------------------------------------");
		for (int i = 0; i < inputItems; i++) {
			System.out.println("Barang Ke : " + (i+1));
			System.out.print("Nama : ");
			String itemName = scanner.next();
			itemsArr[i] = itemName;
			System.out.print("Harga : ");
			int itemAmount = scanner.nextInt();
			amountArr[i] = itemAmount;
		}
		System.out.println("------------------------------------------------------");
		System.out.println("DAFTAR BELANJA");
		System.out.println("==========================");
		System.out.println("No   Nama                     Harga");
		int totalAmount = 0;
		for (int i = 0; i < inputItems; i++) {
		System.out.println((i+1) + "    " + itemsArr[i] + "                        " + amountArr[i]);
			totalAmount += amountArr[i];
		}
		System.out.println("------------------------------------------------------");
		System.out.println("Jadi Total Harga yanng Harus Dibayar : Rp " + totalAmount);
	}
	
}